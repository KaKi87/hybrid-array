# `hybrid-array`

[![](https://shields.kaki87.net/npm/v/hybrid-array)](https://www.npmjs.com/package/hybrid-array)
![](https://shields.kaki87.net/npm/dw/hybrid-array)
![](https://shields.kaki87.net/npm/l/hybrid-array)

An `Array` with `Object` benefits : enjoy `data[id]` as much as `data[index]` & `data.find(item)`.

## Getting started

### Installation

#### From browser

```html
<script type="module">
    import {
        createImmutableHybridArray,
        createLiteHybridArray,
        createHybridArray
    } from 'https://cdn.jsdelivr.net/npm/hybrid-array/mod.js';
</script>
```

#### From Deno

```js
import {
    createImmutableHybridArray,
    createLiteHybridArray,
    createHybridArray
} from 'https://git.kaki87.net/KaKi87/hybrid-array/raw/branch/master/mod.js';
```

#### From Node

```js
import {
    createImmutableHybridArray,
    createLiteHybridArray,
    createHybridArray
} from 'hybrid-array';
```

### Usage

#### For static data

Create an "immutable" hybrid array :

```js
const hybridArray = createImmutableHybridArray(
    data, // required array of objects containing the primary key
    primaryKey // 'id' by default
);
```

Access items :

```js
console.log(hybridArray[index]);
console.log(hybridArray[id]); // statically pointing to the object containing the same ID
```

<details>
<summary>Safety (TL;DR : duplicate IDs, ID edits and items adding/removal should be avoided)</summary>

- When an ID is duplicated, all instances of it are accessible by index, and only the last one is accessible by ID ;
- When an ID is edited, the old ID will still point to the current item and the new ID will still point to nothing (or to its current item if it's a duplicate) ;
- When an item is replaced by index, its current ID pointer will still point to it even if the new ID is different ;
- When an item is replaced by ID, its current index pointer will still point to its current item.
- When an item is added by index, it will have no ID pointer ;
- When an item is added by ID, it will have no index pointer ;
- When an item is deleted by index, its ID reference will still exist ;
- When an item is deleted by ID, its index reference will still exist.

</details>

#### For dynamic but trusted and small data

Create a "lite" hybrid array :

```js
const hybridArray = createLiteHybridArray(
    data, // optional array of objects containing the primary key
    primaryKey // 'id' by default
);
```

Add (more) items :

```js
hybridArray.push(item);
hybridArray[index] = item;
```

Access items :

```js
console.log(hybridArray[index]);
console.log(hybridArray[id]); // runs `hybridArray.find(item => item.id === id)` under the hood
```

<details>
<summary>Safety (TL;DR : duplicate IDs & ID edits should be avoided)</summary>

- When an ID is duplicated, either initially or by edit, all instances of it are accessible by index, and only the lowest-index one is accessible by ID ;
- When an item is added by ID, it will have no index pointer ;
- When an item is deleted by ID, its index pointer will remain.

</details>

#### For dynamic and untrusted or bug data

Create a "fully-featured" hybrid array :

```js
const hybridArray = createHybridArray(
    data, // optional array of objects containing the primary key
    primaryKey // 'id' by default
);
```

Add (more) items :

```js
hybridArray.push(item);
hybridArray[index] = item;
hybridArray[id] = item;
```

Access items :

```js
console.log(hybridArray[index]);
console.log(hybridArray[id]); // uses a Map between ID & index under the hood
```

Delete items :

```js
delete hybridArray[index]; // also removes the ID reference
delete hybridArray[id]; // also removes the index reference
```

<details open>
<summary>Safety</summary>

- When an ID is duplicated, either initially or by edit, only the last instance of it is accessible by index or ID, the previous ones are overwritten.

</details>

## Related projects

- [`junosuarez/indexed-array`](https://github.com/junosuarez/indexed-array)
- [`zeke/keyed-array`](https://github.com/zeke/keyed-array)

## Detailed feature comparison

- `index` & `id` are existing ones
- `nIndex` & `nId` are **n**ew ones
- `eIndex` & `eId` are **e**xisting ones but different from `index` & `id`
- `neIndex` & `neId` are **n**ew or **e**xisting ones (but different from `index` & `id`)
- `{ id }` can contain any additional key/value pairs
- `{}` contains any key/value pairs except `id`

|                               | `Object`           | `Array`            | `createImmutableHybridArray` | `createLiteHybridArray` | `createHybridArray` | `junosuarez/indexed-array` | `zeke/keyed-array` |
|-------------------------------|--------------------|--------------------|------------------------------|-------------------------|---------------------|----------------------------|--------------------|
| ▼ Read operations             |                    |                    |                              |                         |                     |                            |                    |
| `data[index]`                 | :x:                | :white_check_mark: | :white_check_mark:           | :white_check_mark:      | :white_check_mark:  | :white_check_mark:         | :white_check_mark: |
| `data[id]`                    | :white_check_mark: | :x:                | :white_check_mark:           | :white_check_mark:      | :white_check_mark:  | :white_check_mark:         | :white_check_mark: |
| `data.length`                 | :x:                | :white_check_mark: | :white_check_mark:           | :white_check_mark:      | :white_check_mark:  | :white_check_mark:         | :white_check_mark: |
| `JSON.stringify(data)`        | :white_check_mark: | :white_check_mark: | :white_check_mark:           | :white_check_mark:      | :white_check_mark:  | :white_check_mark:         | :white_check_mark: |
| `data[index].id = nId`        | :x:                | :white_check_mark: | :x:                          | :white_check_mark:      | :white_check_mark:  | :x:                        | :x:                |
| ▼ Write operations            |                    |                    |                              |                         |                     |                            |                    |
| `data[index].id = eId`        | :x:                | :x:                | :x:                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| `data[id].id = nId`           | :x:                | :x:                | :x:                          | :white_check_mark:      | :white_check_mark:  | :x:                        | :x:                |
| `data[id].id = eId`           | :x:                | :x:                | :x:                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| `data[neIndex] = { id: nId }` | :x:                | :white_check_mark: | :x:                          | :white_check_mark:      | :white_check_mark:  | :x:                        | :x:                |
| `data[index] = { id }`        | :x:                | :x:                | :x:                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| `data[neId] = {}`             | :white_check_mark: | :x:                | :x:                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| `data.push({ id: nId })`      | :x:                | :white_check_mark: | :x:                          | :white_check_mark:      | :white_check_mark:  | :x:                        | :x:                |
| `data.push({ id })`           | :x:                | :x:                | :x:                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| `delete data[index]`          | :x:                | :white_check_mark: | :x:                          | :white_check_mark:      | :white_check_mark:  | :x:                        | :x:                |
| `delete data[id]`             | :white_check_mark: | :x:                | :x:                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| ▼ Transforming write ops.     |                    |                    |                              |                         |                     |                            |                    |
| `data.splice()`               | :x:                | :white_check_mark: | :x:                          | :white_check_mark:      | :white_check_mark:  | :x:                        | :x:                |
| `data.sort()`                 | :x:                | :white_check_mark: | :white_check_mark:           | :white_check_mark:      | :white_check_mark:  | :white_check_mark:         | :white_check_mark: |
| ▼ Misc                        |                    |                    |                              |                         |                     |                            |                    |
| Input control                 | :x:                | :x:                | N/A                          | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| Unique primary key constraint | :white_check_mark: | :x:                | :white_check_mark:           | :x:                     | :white_check_mark:  | :x:                        | :x:                |
| Custom primary key            | N/A                | N/A                | :white_check_mark:           | :white_check_mark:      | :white_check_mark:  | :white_check_mark:         | :x:                |