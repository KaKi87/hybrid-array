/**
 * Create an immutable hybrid array.
 * @param {object[]} data
 * @param {string} [primaryKey="id"]
 * @returns {object[]}
 */
export default (
    data,
    primaryKey = 'id'
) => Object.defineProperties(
    data,
    data.reduce((
        properties,
        item
    ) => ({
        ...properties,
        [item[primaryKey]]: {
            value: item,
            enumerable: false
        }
    }), {})
);