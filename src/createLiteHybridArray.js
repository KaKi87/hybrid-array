/**
 * Create a basically mutable hybrid array.
 * @param {object[]} [data]
 * @param {string} [primaryKey="id"]
 * @returns {object[]}
 */
export default (
    data = [],
    primaryKey = 'id'
) => new Proxy(data, {
    get: (array, property, receiver) =>
        Reflect.get(array, property, receiver)
        ??
        array.find(item => item?.[primaryKey] === property)
});