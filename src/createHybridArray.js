const isIntegerString = string => /^\d+$/.test(string);

/**
 * Create a highly mutable hybrid array.
 * @param {object|object[]} [data]
 * @param {string} [primaryKey="id"]
 * @returns {object[]}
 */
export default (
    data,
    primaryKey = 'id'
) => {
    const
        index = new Map(),
        proxy = new Proxy([], {
            get: (array, property, receiver) =>
                Reflect.get(array, property, receiver)
                ??
                array[index.get(property)],
            set: (array, property, item, receiver) => {
                if(array[property]){
                    if(isIntegerString(property)){
                        const
                            oldPrimaryValue = array[property][primaryKey],
                            primaryValue = item[primaryKey];
                        if(oldPrimaryValue !== primaryValue){
                            delete array[index.get(primaryValue)];
                            index.delete(oldPrimaryValue);
                            index.set(primaryValue, property);
                        }
                    }
                    if(property !== 'length' || !!array[property - 1])
                        Reflect.set(array, property, item, receiver);
                }
                else if(typeof property === 'string'){
                    const primaryValue = item[primaryKey];
                    if(isIntegerString(property)){
                        if(!primaryValue) throw new Error('INVALID_PRIMARY');
                        const existingIndex = index.get(primaryValue);
                        if(existingIndex)
                            property = existingIndex;
                        else
                            index.set(primaryValue, property);
                        Reflect.set(array, property, new Proxy(
                            item,
                            {
                                get: (...args) => Reflect.get(...args),
                                set: (item, key, value, receiver) => {
                                    if(key === primaryKey){
                                        const existingIndex = index.get(value);
                                        if(existingIndex)
                                            delete array[existingIndex];
                                        index.delete(item[key]);
                                        index.set(value, property);
                                    }
                                    return Reflect.set(item, key, value, receiver);
                                }
                            }
                        ), receiver);
                    }
                    else {
                        if(primaryValue){
                            if(primaryValue !== property)
                                throw new Error('INVALID_PRIMARY');
                        }
                        else
                            item[primaryKey] = property;
                        proxy.push(item);
                    }
                }
                return true;
            },
            deleteProperty: (array, property) => {
                if(isIntegerString(property)){
                    if(array[property]){
                        index.delete(array[property][primaryKey]);
                        delete array[property];
                    }
                }
                else {
                    delete array[index.get(property)];
                    index.delete(property);
                }
                return true;
            }
        });
    if(data)
        Object.assign(proxy, data);
    return proxy;
};